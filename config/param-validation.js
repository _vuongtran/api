import Joi from 'joi';

export default {
  // POST /api/auth/register
  register: {
    body: {
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(30).required(),
      name: Joi.string().min(3).max(30).required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().email().required(),
      password: Joi.string().required()
    }
  }
};
