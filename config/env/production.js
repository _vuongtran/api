export default {
  env: 'production',
  jwtSecret: '0a6b944d-d2fb-4321-a85e-0295c986cd9f',
  db: 'mongodb://localhost/express-mongoose-es6-rest-api-production',
  port: 4040
};
