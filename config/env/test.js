export default {
  env: 'test',
  jwtSecret: '0a6b944d-d2fb-7890-a85e-0295c986cd9f',
  db: 'mongodb://localhost/express-mongoose-es6-rest-api-test',
  port: 4040
};
