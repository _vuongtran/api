export default {
    jobTypeEnum: {
        FULL_TIME: 1,
        PART_TIME: 2,
        REMOTE: 3,
        properties: {
            1: { name: "Full time", value: 1 },
            2: { name: "Part time", value: 2 },
            3: { name: "Remote", value: 3 }
        }
    },
    jobStatusEnum: {
        NEW: 1,
        APPROVED: 2,
        EXPIRED: 3,
        properties: {
            1: { name: "New", value: 1 },
            2: { name: "Approved", value: 2 },
            3: { name: "Expired", value: 3 }
        }
    }
}
