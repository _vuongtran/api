import express from 'express';
import jobCtrl from '../controllers/job.controller';
import middleware from '../middlewares';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
    /** GET /api/jobs - Get list of jobs */
    .get(jobCtrl.list)

/** POST /api/jobs - Create new job */
.post(middleware.authentication, jobCtrl.create);


router.route('/:slug')
    /** GET /api/jobs/:jobId - Get single job */
    .get(jobCtrl.get)

/** PUT /api/jobs/:jobId - Update job */
.put(middleware.authentication, jobCtrl.update)

/** DELETE /api/jobs/:jobId - Delete job */
.delete(middleware.authentication, jobCtrl.remove);

export default router;
