import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../../config/param-validation';
import authCtrl from '../controllers/auth.controller';
import config from '../../config/env';
import middleware from '../middlewares';

const router = express.Router(); // eslint-disable-line new-cap

/** 
	POST /api/auth/login
*/
router.route('/login')
    .post(
        validate(paramValidation.login),
        authCtrl.login);

/** 
	POST /api/auth/logout 
*/
router.route('/logout')
    .post(middleware.authentication, authCtrl.logout);

/** 
	POST /api/auth/register 
*/
router.route('/register')
    .post(
        validate(paramValidation.register),
        middleware.checkEmailExist,
        authCtrl.register
    );

export default router;
