import jwt from 'jsonwebtoken';
import APIError from '../helpers/APIError';
import httpStatus from 'http-status';
import defaultValue from '../../config/default-value';
import User from '../models/user.model';

const config = require('../../config/env');

function authentication(req, res, next) {
    const token = req.body.token || req.headers['authorization'];
    if (token) {
        jwt.verify(token, config.jwtSecret, (err, decoded) => {
            if (err) {
                return next(err);
            }
            const email = decoded.email; // eslint-disable-line no-underscore-dangle
            User.findOne({ email: email }, (error, user) => {
                if (error) {
                    return next(error);
                }
                if (!user) {
                    const error = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
                    return next(error);
                }
                req.user = user;
                return next();
            });
        });
    } else {
        const error = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
        return next(error);
    }
}

function checkEmailExist(req, res, next) {
    const email = req.body.email;
    User.findOne({ email: email }, (error, user) => {
        if (error) {
            return next(error);
        }
        if (user) {
            const error = new APIError('Email already exists', httpStatus.CONFLICT);
            return next(error);
        }
        return next();
    });
}

export default {
    authentication,
    checkEmailExist,
};
