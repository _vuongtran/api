import jwt from 'jsonwebtoken';
const config = require('../../config/env');

function getToken(email) {
    const token = jwt.sign({
        email: email
    }, config.jwtSecret);
    return token;
}

export default { getToken };
