import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Helper from '../helpers';
import User from '../models/user.model';

const config = require('../../config/env');

function login(req, res, next) {
    const email = req.body.email;
    const password = req.body.password;
    const query = { email: email };
    User.findOne(query, (err, user) => {
        if (err || !user) {
            const error = new APIError('Authentication error', httpStatus.UNAUTHORIZED);
            return next(error);
        }
        user.comparePassword(password, (error, isMatch) => {
            if (!isMatch || error) {
                return res.status(401).json({
                    success: false,
                    message: 'Authentication failed. Password did not match.'
                });
            }
            const token = Helper.getToken(user.email);
            user.token = token;
            user.save()
                .then((savedUser) => {
                    return User.findOneById(savedUser._id);
                })
                .then((foundUser) => {
                    res.status(200).json({
                        success: true,
                        data: foundUser
                    });
                })
                .catch((e) => {
                    return next(e)
                });
        });
    });
}

function logout(req, res, next) {
    const userId = req.user._id;
    User.findOneById(userId)
        .then((user) => {
            user.token = null;
            return user.save();
        })
        .then(() => {
            res.json({
                success: true
            })
        })
        .catch(e => next(e));
}

function register(req, res, next) {
    const token = Helper.getToken(req.body.email);
    const user = new User({
        email: req.body.email,
        password: req.body.password,
        name: req.body.name,
        token: token
    });
    user.save()
        .then((savedUser) => {
            res.json({
                success: true,
                user: user
            })
        })
        .catch(e => next(e));
}

export default { login, logout, register };
