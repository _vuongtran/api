import uuid from 'uuid';
import Job from '../models/job.model';
import defaultValue from '../../config/default-value';

function create(req, res, next) {
    let newJob = new Job({
        slug: uuid.v1(),
        title: req.body.title,
        location: req.body.location,
        type: req.body.type,
        description: req.body.description,
        how_to_apply: req.body.how_to_apply,
        company_name: req.body.company_name,
        company_url: req.body.company_url,
        company_logo: req.body.company_logo,
        url: req.body.url,
        status: defaultValue.jobStatusEnum.NEW,
        user_created: req.user._id
    });
    newJob.save()
        .then((savedJob) => {
            return res.json({
                success: true,
                data: savedJob
            });
        })
        .catch((e) => {
            return next(e);
        });
}

function list(req, res, next) {
    let page = parseInt(req.query.page || 1);
    let itemPerPage = parseInt(req.query.item_per_page || 10);
    const skip = (itemPerPage * (page - 1));
    const limit = itemPerPage;
    Job.list(skip, limit)
        .then((jobs) => {
            return res.json({
                success: true,
                data: {
                    page: page,
                    item_per_page: itemPerPage,
                    jobs: jobs
                }
            });
        })
        .catch((e) => {
            return next(e);
        })
}

function get(req, res, next) {
    const slug = req.params.slug;
    Job.findBySlug(slug)
        .then((job) => {
            return res.json({
                success: true,
                data: job
            });
        })
        .catch((e) => {
            return next(e);
        });
}

function update(req, res, next) {
    const slug = req.params.slug;
    Job.findBySlug(slug)
        .then((job) => {
            job.title = req.body.title;
            job.location = req.body.location;
            job.type = req.body.type;
            job.description = req.body.description;
            job.how_to_apply = req.body.how_to_apply;
            job.company_name = req.body.company_name;
            job.company_url = req.body.company_url;
            job.company_logo = req.body.company_logo;
            //job.url = req.body.url;
            //job.status = defaultValue.jobStatusEnum.NEW;
            //job.user_created = req.user._id
            return job.save();
        })
        .then((job) => {
            return res.json({
                success: true,
                data: job
            });
        })
        .catch((e) => {
            return next(e);
        });
}

function remove(req, res, next) {
    const slug = req.params.slug;
    Job.findBySlug(slug)
        .then((job) => {
            return job.remove();
        })
        .then((removedJob) => {
            return res.json({
                success: true,
                data: removedJob
            });
        })
        .catch((e) => {
            return next(e);
        })
}

export default { create, list, get, update, remove };
