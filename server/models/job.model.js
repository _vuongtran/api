import mongoose from 'mongoose';
var Schema = mongoose.Schema;

const JobSchema = new mongoose.Schema({
    slug: {
        type: String,
        required: true,
        unique: true
    },
    title: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    type: {
        type: Number,
        required: true
    },
    status: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    how_to_apply: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    company_name: {
        type: String,
        required: true
    },
    company_url: {
        type: String,
        required: true
    },
    company_logo: {
        type: String,
        required: true
    },
    user_created: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

JobSchema.statics = {
    findById(id) {
        return this.findById(id)
            .exec();
    },
    findBySlug(slug) {
        return this.findOne({ slug: slug })
            .exec();
    },
    list(skip, limit) {
        return this.find()
            .sort({ created_at: -1 })
            .skip(skip)
            .limit(limit)
            .exec();
    }
}

export default mongoose.model('Job', JobSchema);
